import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class NewFileDerctoryConstructorService {
    public fileCounter = 1;
    public dirCounter = 1;
}
import { Injectable } from '@angular/core';
import { IFileSystemDatacache, IFileSystemDataRender } from '../interfaces/fileSystemData.interface';

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  public cache: IFileSystemDatacache[] = [];

  public getLastVisitedDir(): IFileSystemDatacache {
    return this.cache.find(c => c.current);
  }

  public getCacheLastEl(): IFileSystemDatacache {
    return this.cache[this.cache.length - 1];
  }

  public addToCache(fileSystemDataRender: IFileSystemDataRender): void {
    const lastCacheEl = this.getCacheLastEl();

    const lastActive = this.getLastVisitedDir();
    if (lastActive) {
      lastActive.current = false;
    }

    const cacheNewItem: IFileSystemDatacache = {
      ...fileSystemDataRender,
      current: true
    };

    this.cache.push(cacheNewItem);
  }

  public canCacheGoBack(): boolean {
    return !!this.getLastVisitedDir();
  }

  public canCacheForward(): boolean {
    return !!this.cache.length && !this.getCacheLastEl().current;
  }

  public goBack(): IFileSystemDatacache {
    const indexOfLastVisitedDir: number = this.cache.findIndex(i => i.current);

    if (this.cache[indexOfLastVisitedDir]) {
      this.cache[indexOfLastVisitedDir].current = false;

      if (this.cache[indexOfLastVisitedDir - 1]) {
        this.cache[indexOfLastVisitedDir - 1].current = true;
        return this.getLastVisitedDir();
      }
    }

    return null;
  }

  public goForward(): IFileSystemDatacache {
    const indexOfLastVisitedDir: number = this.cache.findIndex(i => i.current);

    if (!this.cache[indexOfLastVisitedDir] && this.cache.length) {
      this.cache[0].current = true;
      return this.getLastVisitedDir();
    }

    if (this.cache[indexOfLastVisitedDir + 1]) {
      this.cache[indexOfLastVisitedDir].current = false;
      this.cache[indexOfLastVisitedDir + 1].current = true;
      return this.getLastVisitedDir();
    }

    return null;
  }
}

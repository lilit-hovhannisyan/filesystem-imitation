import { Component, Input, OnInit } from '@angular/core';
import { getFileExtension } from '../../helpers/random.helper';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.less']
})
export class FileComponent implements OnInit {
  @Input() public fileName: string;
  public fileIcon: string;

  constructor() { }

  public ngOnInit(): void {
    this.fileIcon = 'icon-file-' + getFileExtension(this.fileName);
  }
}

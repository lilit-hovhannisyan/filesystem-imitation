import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.less']
})
export class BreadcrumbComponent implements OnInit, OnChanges {
  @Input() public path: string;
  public pathArr: string[] = [];

  @Output() public getPortion = new EventEmitter<string>();

  constructor() { }

  public ngOnInit(): void {
    this.initState();
  }

  public ngOnChanges(): void {
    this.initState();
  }

  private initState(): void {
    if (this.path) {
      this.pathArr = this.path?.split('/');
    }
  }

  public onGetPortion(str: string): void {
    this.getPortion.emit(str);
  }
}

import { Component, OnInit } from '@angular/core';
import { fileSystemData } from '../../data/fileSystemData';
import { IFileSystemDataRender } from '../../interfaces/fileSystemData.interface';
import { CacheService } from '../../services/cache.service';
import { getFileExtension } from '../../helpers/random.helper';
import { IButton } from '../../interfaces/button.interface';
import { MODE } from '../../enums/random.enums';
import { NewFileDerctoryConstructorService } from '../../services/new-file-derctory.constructor.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-file-system',
  templateUrl: './file-system.component.html',
  styleUrls: ['./file-system.component.less'],
})
export class FileSystemComponent implements OnInit {
  // data
  public fileSystemDataSource: Map<string, IFileSystemDataRender> = new Map<string, IFileSystemDataRender>();
  public fileSystemDataSourceArray: IFileSystemDataRender[];
  public fileSystemData: IFileSystemDataRender[];
  public currentDirectoryFullPath: string;
  private baseDirName = 'baseDirectory';

  // top bar
  public canGoBack: boolean;
  public canGoForward: boolean;
  public MODE = MODE;
  public mode: MODE = MODE.menu;
  public topBarBtns: IButton[];
  public tableHeaders: string[] = ['Name', 'Date Modified', 'Size', 'Kind'];

  // search
  public searchValue = '';

  constructor(
    public cacheService: CacheService,
    public newFileDerctoryConstructorService: NewFileDerctoryConstructorService,
    private router: Router,
  ) {}

  public ngOnInit(): void {
    this.init();
  }

  // data manipulation
  private initFileSystemDataSource(): void {
      fileSystemData
        .forEach(i => {
          let parent: string;
          let name: string;
          let fullPath: string;

          if (i.path.indexOf('/') === -1) {
            parent = null;
            name = i.path;
            fullPath = this.baseDirName + '/' + name ;

            this.fileSystemDataSource.set(name, {
              parent,
              name,
              fullPath,
              ...i
            });
          } else {
            const pathArr = i.path.split('/');

            for ( let index = 1; index < pathArr.length; index++) {
              parent = pathArr[index - 1];
              name = pathArr[index];
              fullPath = this.baseDirName + '/' + pathArr.slice(0, index + 1).join('/');

              this.fileSystemDataSource.set(name, {
                  parent,
                  name,
                  fullPath,
                  ...i,
                  type: name.indexOf('.') >= 0 ? 'file' : 'folder'
              });
            }
          }
        });
  }

  private init(): void {
    this.initFileSystem();
    this.initTopBtns();
    this.setUrl();
  }

  private initRoot(): void {
    this.fileSystemData = this.fileSystemDataSourceArray.filter(i => i.parent === null);
  }

  private initFileSystem(): void {
    this.initFileSystemDataSource();
    this.fileSystemData = [];

    const lastVisitedDir = this.cacheService.getLastVisitedDir();
    this.currentDirectoryFullPath = this.cacheService.getLastVisitedDir() ?
      lastVisitedDir.fullPath : this.baseDirName;

    this.fileSystemDataSourceArray = Array.from(this.fileSystemDataSource.values());

    if (!lastVisitedDir || !lastVisitedDir.current) {

       this.initRoot();
       return;
    }

    this.fileSystemData = this.fileSystemDataSourceArray.filter(i => i.parent === lastVisitedDir.name);
  }

  private initTopBtns(): void {
    this.canGoBack = this.cacheService.canCacheGoBack();
    this.canGoForward = this.cacheService.canCacheForward();

    this.topBarBtns  = [
      {
        icon: 'arrow_left',
        action: this.onBack,
        customClass: this.canGoBack ? 'allowed' : 'noEvent'
      },
      {
        icon: 'arrow_right',
        action: this.onForward,
        customClass: this.canGoForward ? 'navControl allowed' : 'navControl noEvent'
      },
      {
        icon: 'list',
        action: this.onShowAs,
        customClass: this.mode === MODE.list ? 'active noEvent' : ''
      },
      {
        icon: 'menu',
        action: this.onShowAs,
        customClass: this.mode === MODE.menu ? 'active noEvent viewControl' : 'viewControl'
      },
      {
        icon: 'add',
        text: 'Add file',
        action: this.onAddFile
      },
      {
        icon: 'add',
        text: 'Add folder',
        action: this.onAddDir
      }
    ];
  }

  // helper functions
  private isFile(elName: string): boolean {
    return elName.indexOf('.') >= 0;
  }

  public renderType(el: IFileSystemDataRender): string {
      const path = el.path;
      const name = path.substr(path.lastIndexOf('/') + 1); // todo
      return el.type === 'folder' ?
          'Folder' :
          getFileExtension(name).toUpperCase() + ' document';
  }

  // directory dblClick
  public onDirectoryDblClick(dirName: string, el: IFileSystemDataRender ): void {
    this.cacheService.addToCache(el);
    this.init();
  }

  private setUrl(): void {
    const urlPath: string = this.currentDirectoryFullPath === this.baseDirName
        ? ''
        : this.currentDirectoryFullPath.substr(this.currentDirectoryFullPath.indexOf('/'));

    this.router.navigate([urlPath]);
  }

  // top bar actions
  private onBack = (): void => {
    this.cacheService.goBack();
    this.init();
  }

  private onForward = (): void => {
      this.cacheService.goForward();
      this.init();
  }

  private onShowAs = (btn: IButton): void => {
    this.mode = MODE[btn.icon];
    this.initTopBtns();
  }

  private onAddFile = (): void => {
    let dateStr: string = new Date().toISOString();
    dateStr = dateStr.substr(0, dateStr.indexOf('T'));

    const fileName = 'new-file-' + this.newFileDerctoryConstructorService.fileCounter++ + '.unknown';
    const parent = this.currentDirectoryFullPath.indexOf('/') === -1 ?
        null :
        this.currentDirectoryFullPath.substr(this.currentDirectoryFullPath.lastIndexOf('/') + 1);

    this.fileSystemDataSource.set(fileName, {
      path: this.currentDirectoryFullPath + '/' + fileName,
      modificationDate: dateStr,
      type: 'file',
      size: 0,
      parent,
      name: fileName,
      fullPath: this.currentDirectoryFullPath + '/' + fileName
    });

    this.initFileSystem();
  }

  private onAddDir = (btn): void => {
    let dateStr: string = new Date().toISOString();
    dateStr = dateStr.substr(0, dateStr.indexOf('T'));

    const dirName = 'new-folder-' + this.newFileDerctoryConstructorService.dirCounter++;
    const parent = this.currentDirectoryFullPath.indexOf('/') === -1 ?
        null :
        this.currentDirectoryFullPath.substr(this.currentDirectoryFullPath.lastIndexOf('/') + 1);

    this.fileSystemDataSource.set(dirName, {
        path: this.currentDirectoryFullPath + '/' + dirName,
        modificationDate: dateStr,
        type: 'folder',
        parent,
        name: dirName,
        fullPath: this.currentDirectoryFullPath + '/' + dirName
    });

    this.initFileSystem();
  }

  public onJumpToDir(dirName: string): void {
    if (dirName === this.baseDirName) {
      this.initRoot();
      this.roolbackstateOnJumpDoDir();
      return;
    }
    const el: IFileSystemDataRender = this.fileSystemDataSourceArray.find(i => i.name === dirName);
    this.onDirectoryDblClick(dirName, el);
  }

  private roolbackstateOnJumpDoDir(): void {
    this.currentDirectoryFullPath = this.baseDirName;
    if (this.cacheService.getLastVisitedDir()) {
      this.cacheService.getLastVisitedDir().current = false;
    }
    this.setUrl();
    this.initTopBtns();
  }

  // search
  public onSearchChange(str: string): void {
    this.fileSystemData = [];

    if (!str) {
      this.initRoot();
      this.roolbackstateOnJumpDoDir();
      return;
    }

    this.fileSystemData = this.fileSystemDataSourceArray
        .filter(i => i.name.toLowerCase().indexOf(str.toLowerCase()) >= 0);
  }
}

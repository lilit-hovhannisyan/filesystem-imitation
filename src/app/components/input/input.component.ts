import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.less']
})
export class InputComponent implements OnInit {
  @Input() public inputIcon: string;
  @Input() public value: string;

  @Output() public inputEv = new EventEmitter<string>();

  constructor() { }

  public ngOnInit(): void {
  }

  public onInput(event: any): void {
    this.inputEv.emit(event.target.value);
  }
}

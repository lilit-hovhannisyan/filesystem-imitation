import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.less']
})
export class ButtonComponent implements OnInit {
  @Input() public btnName: string;
  @Input() public btnIcon: string;

  @Output() public clickEv = new EventEmitter<MouseEvent>();

  constructor() { }

  public ngOnInit(): void {
  }

  public onClick(e: MouseEvent): void {
    this.clickEv.emit(e);
  }
}

import { Component, OnInit } from '@angular/core';
import { IButton } from '../../interfaces/button.interface';

@Component({
  selector: 'app-finder',
  templateUrl: './finder.component.html',
  styleUrls: ['./finder.component.less']
})
export class FinderComponent implements OnInit {
  public finderActions: IButton[];

  constructor() { }

  public ngOnInit(): void {
    this.initBtns();
  }

  private initBtns(): void {
    this.finderActions = [
      {
        icon: 'close',
        action: this.closeFinder
      },
      {
        icon: 'minus',
        action: this.minimizeFinder
      },
      {
        icon: 'expand',
        action: this.expandFinder
      }
    ];
  }

  private closeFinder = (): void => {
    alert('not working yet');
  }

  private minimizeFinder = (): void => {
      alert('not working yet');
  }

  private expandFinder = (): void => {
      alert('not working yet');
  }

}

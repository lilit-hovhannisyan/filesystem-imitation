import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.less']
})
export class DirectoryComponent implements OnInit {
  @Input() public dirName: string;
  public dirIcon = 'icon-folder';

  @Output() public dblClickEv = new EventEmitter<string>();

  constructor() { }

  public ngOnInit(): void {
  }

  public onDblClick(e: MouseEvent): void {
    this.dblClickEv.emit(this.dirName);
  }
}

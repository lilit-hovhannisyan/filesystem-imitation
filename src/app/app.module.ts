import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DirectoryComponent } from './components/directory/directory.component';
import { ButtonComponent } from './components/button/button.component';
import { IconComponent } from './components/icon/icon.component';
import { FileComponent } from './components/file/file.component';
import { FileSystemComponent } from './components/file-system/file-system.component';
import { InputComponent } from './components/input/input.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { FinderComponent } from './components/finder/finder.component';

@NgModule({
  declarations: [
    AppComponent,
    FileComponent,
    DirectoryComponent,
    FileSystemComponent,
    ButtonComponent,
    IconComponent,
    InputComponent,
    BreadcrumbComponent,
    FinderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export const getFileExtension = (fileName: string): string => {
  return fileName.substr(fileName.indexOf('.') + 1);
}

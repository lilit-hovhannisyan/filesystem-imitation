export interface IButton {
  text?: string;
  icon?: string;
  customClass?: string;
  action?: (btn?: any) => void;
}

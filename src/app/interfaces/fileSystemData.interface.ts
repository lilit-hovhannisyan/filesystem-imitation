export interface IFileSystemData {
  path: string;
  modificationDate: string;
  type: 'folder' | 'file';
  size?: number;
}

export interface IFileSystemDataRender extends IFileSystemData {
  parent: string | null;
  name: string;
  fullPath: string;
}

export interface IFileSystemDatacache extends IFileSystemDataRender {
  current: boolean;
}
